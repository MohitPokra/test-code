<?php

$servername = "localhost";
$username = "username";
$password = "password";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE IF NOT EXISTS `myDb`";
if ($conn->query($sql) === TRUE) {

    echo "Database created successfully \n";
    $sql = "CREATE TABLE IF NOT EXISTS `mydb`.`records`(
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	words VARCHAR(30) NOT NULL
	)";

    if ($conn->query($sql) === TRUE) {

        echo "Table records created successfully \n";
        $array = readTextfile();
        $values = array();

        foreach ($array as $row) {
            $values[] = '("' . $row . '")';
        }

        $sql = "INSERT INTO `mydb`.`records` (words) VALUES" . implode(',', $values) . ';';
        $msg = "Text file words inserted successfully \n";
        sqlMsg($conn, $sql, $msg);

        echo "Distinct unique words: " . count(array_filter($array)) . "\n";

    } else {
        echo "Error creating table: " . $conn->error . "\n";
    }

    $sql = "CREATE TABLE IF NOT EXISTS `mydb`.`watchlist`(
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	words VARCHAR(30) NOT NULL
	)";

    if ($conn->query($sql) === TRUE) {

        echo "Table records created successfully \n";
        $values = WatchlistWords();
        $sql = "INSERT INTO `mydb`.`watchlist` (words) VALUES" . implode(',', $values) . ';';
        $msg = "Watch-list record inserted successfully \n";
        sqlMsg($conn, $sql, $msg);

        $sql = "SELECT DISTINCT `mydb`.`records`.`words`
        FROM `mydb`.`records`, `mydb`.`watchlist` 
        WHERE `mydb`.`records`.`words` = `mydb`.`watchlist`.`words`";

        showResult($conn, $sql);
    }
} else {
    echo "Error creating table: " . $conn->error . "\n";
}

$conn->close();

function readTextFile()
{
    $fh = fopen('prog.txt', 'r');
    $array = array();

    while ($line = fgets($fh)) {
        $line = preg_replace('/\s\s+/', ' ', $line);
        $wordsPerLine = explode(" ", $line);
        foreach ($wordsPerLine as $word) {
            array_push($array, $word);
        }
    }

    $data = array_unique(array_filter($array));
    fclose($fh);

    return $data;
}

function sqlMsg($conn, $sql, $msg)
{
    if ($conn->query($sql) === TRUE) {
        echo $msg;
    } else {
        echo "Error: " . $sql . " " . $conn->error;
    }
}

function showResult($conn, $sql)
{
    if ($result = $conn->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            printf("\n%s \n", $row["words"]);
        }
        /* free result set */
        $result->close();
    } else {
        echo "Error: " . $sql . " " . $conn->error;
    }
}

function watchListWords()
{
    $values = array();
    $data = ['flew ', 'head ', 'found', 'neck ', 'tain', 'air'];

    foreach ($data as $row) {
        $values[] = '("' . $row . '")';
    }

    return $values;
}

?>